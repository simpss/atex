package utils;

import java.util.Random;

public class TestUtils {
    public static int randomInt(int min, int max){
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        return new Random().nextInt((max - min) + 1) + min;
    }
}
