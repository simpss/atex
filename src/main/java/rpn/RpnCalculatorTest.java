package rpn;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.TestUtils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static utils.TestUtils.randomInt;

public class RpnCalculatorTest {

    private RpnCalculator c;

    @Before
    public void setUp() {
        c = new RpnCalculator();
    }

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void accumulatorValueCanBeSet(){
        int val = 1;
        c.setAccumulator(val);
        assertThat(c.getAccumulator(), is(val));
    }

    @Test
    public void calculatorSupportsAddition() {
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSupportsMultiplying() {
        int var1 = randomInt(1, 10);
        int var2 = randomInt(1, 20);

        c.setAccumulator(var1);
        c.enter();
        c.setAccumulator(var2);
        c.multiply();
        assertThat(c.getAccumulator(), is(var1 * var2));
    }

    @Test
    public void calculatorSupportsTwoActions(){
        int var1 = 1;
        int var2 = 2;
        int var3 = 4;

        c.setAccumulator(var1);
        c.enter();
        c.setAccumulator(var2);
        c.plus();
        c.enter();
        c.setAccumulator(var3);
        c.multiply();
        assertThat(c.getAccumulator(), is((var1 + var2) * var3));
    }

    @Test
    public void calculatorSupportsThreeActions(){
        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();
        assertThat(c.getAccumulator(), is(21));
    }

    @Test
    public void calculatorSupportsStringInput(){
        int res = c.evaluate("5 1 2 + 4 * + 3 +");
        assertThat(res, is(20));
    }

    @Test
    public void stringOnePlusTwoIsThree(){
        int res = c.evaluate("1 2 +");
        assertThat(res, is(3));
    }
}
