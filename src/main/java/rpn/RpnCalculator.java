package rpn;

import java.util.Stack;

public class RpnCalculator {

    private Integer value = 0;
    private Stack<Integer> stack = new Stack<Integer>();

    public int getAccumulator(){
        return value;
    }

    public void setAccumulator(int newValue){
        this.value = newValue;
    }

    public void enter(){
        stack.push(value);
    }

    public void plus(){
        this.value = stack.pop() + value;
    }

    public void multiply(){
        this.value = stack.pop() * value;
    }

    public int evaluate(String expression){
        String[] things = expression.split(" ");

        for (String thing : things) {
            if(tryParseInt(thing)){
                saveNumber(thing);
            }else{
                doAction(thing);
            }
        }

        return this.value;
    }

    private void saveNumber(String nr){
        this.setAccumulator(Integer.parseInt(nr));
        this.enter();
    }

    private void doAction(String ident){
        if(ident.equals("+")){
            this.value = stack.pop();
            this.plus();
            this.enter();
        }
        if(ident.equals("*")){
            this.value = stack.pop();
            this.multiply();
            this.enter();
        }
    }

    private boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
